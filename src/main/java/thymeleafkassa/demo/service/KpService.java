package thymeleafkassa.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import thymeleafkassa.demo.domain.Kp;
import thymeleafkassa.demo.repository.KpRepository;

import java.util.List;

@Service
public class KpService {

    @Autowired
    private KpRepository kpRepository;

    public List<Kp> kpAll() {
        return kpRepository.findAllKp();
    }

    public Kp kpById(int id) {
        return kpRepository.findKpById(id);
    }

    public Kp kpDelById(int id) {
        Kp kp = kpRepository.findKpById(id);
        if (kp != null) {
            kpRepository.delete(kp);
        }
        return kp;
    }

    public Kp kpStorById(int id) {
        Kp kp = kpRepository.findKpById(id);
        if (kp == null) return null;
        kp.setStorno(true);
        kpRepository.save(kp);
        return kp;
    }
}
