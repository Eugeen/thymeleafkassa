package thymeleafkassa.demo.dto;

import lombok.Data;

@Data
public class KpDto {
    private int id;
    private String name;
}
