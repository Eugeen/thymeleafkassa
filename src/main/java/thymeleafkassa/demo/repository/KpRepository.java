package thymeleafkassa.demo.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import thymeleafkassa.demo.domain.Kp;


import java.util.List;

@Repository
public interface KpRepository extends CrudRepository<Kp, Integer> {
    @Query("select k from Kp k order by k.datetime")
    List<Kp> findAllKp();

    @Query("select k from Kp k where k.id = ?1")
    Kp findKpById(int id);

}
