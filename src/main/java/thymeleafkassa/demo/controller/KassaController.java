package thymeleafkassa.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import thymeleafkassa.demo.domain.Kassa;
import thymeleafkassa.demo.domain.Kp;
import thymeleafkassa.demo.dto.KpDto;
import thymeleafkassa.demo.service.KpService;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/kassa")
public class KassaController {

    @Autowired
    private KpService kpService;

    @RequestMapping("/KpOpers")
    public String allKp(Map<String, Object> model) {
        model.put("kps", kpService.kpAll());
        return "KpOpers";
    }

    @PostMapping("/KpOpers")
    public String KpById(Map<String, Object> model, @ModelAttribute KpDto kpDto) {
        model.put("kps", kpService.kpAll());
        model.put("kp", kpDto);
        System.out.println(kpDto);
        return "KpOpers";
    }

    @RequestMapping(value = "/KpOpersDelete/{id}", method = RequestMethod.GET)
    public String deleteOper(@PathVariable int id) {
        kpService.kpDelById(id);
        //  System.out.println(kpService.kpDelById(id));
        System.out.println(id);
        return "redirect:/kassa/KpOpers";
    }

    @RequestMapping(value = "/KpOpersStor/{id}", method = RequestMethod.GET)
    public String storOper(@PathVariable int id) {
        kpService.kpStorById(id);
        return "redirect:/kassa/KpOpers";
    }
}
