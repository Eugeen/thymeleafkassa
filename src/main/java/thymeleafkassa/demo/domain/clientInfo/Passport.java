package thymeleafkassa.demo.domain.clientInfo;

import lombok.Data;

import javax.persistence.Embeddable;

@Embeddable
@Data
public class Passport {
    private String passNum;
    private String passSer;
    private String selfId;
}
