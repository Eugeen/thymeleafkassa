package thymeleafkassa.demo.domain;

import lombok.Data;
import thymeleafkassa.demo.domain.clientInfo.Address;
import thymeleafkassa.demo.domain.clientInfo.Passport;

import javax.persistence.*;

@Entity
@Data
@Table(name = "Clients")
@NamedQuery(name = "clientAll", query = "select cl from Clients cl order by cl.id")
public class Clients {
    @Id
    @GeneratedValue
    private int id;

    @Column(name = "fio",nullable = false)
    private String fio;

    @Embedded
    private Address address;

    @Embedded
    private Passport passport;
}
