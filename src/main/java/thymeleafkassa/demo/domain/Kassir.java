package thymeleafkassa.demo.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "Kassir")
public class Kassir {

    @Id
    @GeneratedValue
    private int id;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "fio")
    private String fio;

    @JoinColumn(name = "filial_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Filial filial;

    @OneToMany(mappedBy = "kassir", fetch = FetchType.LAZY)
    private List<Kp> kpList;

}
