package thymeleafkassa.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThymeleafKassaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThymeleafKassaApplication.class, args);
	}

}

